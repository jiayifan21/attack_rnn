import foolbox
import keras
import numpy as np
from keras import backend
from keras.models import load_model
from keras.datasets import mnist
from keras.utils import np_utils
from foolbox.attacks import SaliencyMapAttack
from foolbox.criteria import Misclassification
import matplotlib.pyplot as plt


backend.set_learning_phase(False)

image, label = foolbox.utils.imagenet_example()

def attack_predict():
	model = keras.models.load_model('mnist.hdf5')
	fmodel = foolbox.models.KerasModel(model, bounds=(0,1))
	_,(x_test, y_test) = mnist.load_data()
	x_test = x_test.reshape(10000,28,28,1)
	x_test= x_test.astype('float32')
	x_test /= 255
	batch_size = 1
	classifier_accuracy = 0
	encoder_accuracy = 0

	for img_no in range(batch_size):
		img_no = 0

		attack=foolbox.attacks.FGSM(fmodel, criterion=Misclassification())
 		adversarial= attack(x_test[img_no],y_test[img_no]) # for single image
		
		adversarial =adversarial.reshape(1,28,28,1)
		original = x_test[img_no].reshape(1,28,28,1)
		
		ori_label = np.argmax(model.predict(original), axis = 1)
		adv_label = np.argmax(model.predict(adversarial), axis = 1)
		
		encoder = load_model("encoder_300000.h5")
		generator = load_model("generator_300000.h5")

		c_z_ori = encoder.predict(original)
		img_cat = np.argmax(c_z_ori[:,0:10], axis =1)
		gen_img_ori = generator.predict(c_z_ori)

		c_z_adv = encoder.predict(adversarial)
		img_cat = np.argmax(c_z_adv[:,0:10], axis =1)
		gen_img_adv = generator.predict(c_z_adv)
		
		for i in range(1):
			if img_cat[i] == 0:
			    img_cat[i] = 3
			elif img_cat[i] == 1:
			    img_cat[i] = 6
			elif img_cat[i] == 2:
			    img_cat[i] = 4
			elif img_cat[i] == 3:
			    img_cat[i] = 0
			elif img_cat[i] == 4:
			    img_cat[i] = 5
			elif img_cat[i] == 5:
			    img_cat[i] = 8
			elif img_cat[i] == 6:
			    img_cat[i] = 9
			elif img_cat[i] == 7:
			    img_cat[i] = 1
			elif img_cat[i] == 8:
			    img_cat[i] = 2
			elif img_cat[i] == 9:
			    img_cat[i] = 7
		
		if adv_label == y_test[img_no]:
			classifier_accuracy += 1
		if img_cat == y_test[img_no]:
			encoder_accuracy += 1

		#print ("ori_label", ori_label)
		#print ("adv_label", adv_label)
		#print ("img_cat", img_cat)
		print (img_no)
	classifier_accuracy_total = classifier_accuracy/batch_size
	encoder_accuracy_total = encoder_accuracy/batch_size

	print ("classifier_accuracy_total", classifier_accuracy_total)
	print ("encoder_accuracy_total", encoder_accuracy_total)
	plt.figure()
	plt.subplot(2,2,1)
	plt.title('Original')
	plt.imshow(original[0,:,:,0],cmap='gray')
	plt.axis('off')

	plt.subplot(2,2,2)
	plt.title('Original Reconstructed')
	plt.imshow(gen_img_ori[0,:,:,0],cmap='gray')
	plt.axis('off')

	plt.subplot(2, 2, 3)
	plt.title('Adversarial')
	plt.imshow(adversarial[0,:,:,0],cmap='gray')
	plt.axis('off')

	plt.subplot(2, 2, 4)
	plt.title('Adversarial Reconstructed')
	plt.imshow(gen_img_adv[0,:,:,0],cmap='gray')
	plt.axis('off')
	plt.show()


attack_predict()






