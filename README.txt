@Yifan: yifan.jia@tuv-sud.sg

Folders:

audio_adversarial_examples-master: 
audio attack paper included with sample code and data (Linux environment)

code and data:
LSTM for SWaT.py: code for RNN detection on CPS
attack_RNN_audio: audio attack code, same as the on in folder audio_adversarial_examples-master (TensorFlow)
attack_RNN_audio: RNN attack code, try to write in Keras
others: models and sample data

Files:

Adversarial_attack_on_RNN_for_CPS.pdf:
Paper to work on, inside include online latex editor link

others:
result comparasion screen shots
