import keras
import numpy as np
import matplotlib.pyplot as plt
from keras.layers import Dense, Dropout, Activation
from keras.models import Sequential
import keras.backend as K
import tensorflow as tf
from keras.models import load_model
import pandas as pd
from sklearn.preprocessing import MinMaxScaler


def DataNormalization(input_data):
    values = input_data
    # train the normalization
    scaler = MinMaxScaler(feature_range=(0, 1))
    scaler = scaler.fit(values)
    normalized = scaler.transform(values)
    return normalized

#n = 100         # sample size
#x = np.linspace(0,1,n)    #input
#y = 4*(x-0.5)**2          #output
#dy = 8*(x-0.5)       #derivative of output wrt the input
#model = Sequential()
#model.add(Dense(8, input_dim=1, activation='relu')) 
#           # 1d input
#model.add(Dense(1))                                             # 1d output
#
## Minimize mse
#model.compile(loss='mse', optimizer='adam', metrics=["accuracy"])
#model.fit(x, y, batch_size=10, epochs=10, verbose=0)
#
#
#a = tf.Variable([[2.0]])
#
#
#print ("____________", a)
#print (model.input)
df_whole_x_sensor = pd.read_csv('attack_x_sensor.csv', encoding='utf-8')
df_whole_x_actuator = pd.read_csv('attack_x_actuator.csv', encoding='utf-8')
headers = list(pd.read_csv('attack_x.csv', nrows=1))
headers_sensor = list(pd.read_csv('attack_x_sensor.csv', nrows=1))
headers_actuator = list(pd.read_csv('attack_x_actuator.csv', nrows=1))
#df_whole_x = pd.read_csv('attack_x.csv', encoding='utf-8')
        
df_whole_x_actuator = df_whole_x_actuator.replace(0,1.5)
df_whole_x = pd.concat([df_whole_x_sensor,df_whole_x_actuator], axis=1, sort=False)
df_whole_x = df_whole_x[headers]



print('step1-Normalizing...')
inputXX = pd.DataFrame(DataNormalization(df_whole_x),columns = headers)
inputX = np.expand_dims(inputXX,axis = 1)


loss = [0.3]*len(inputX)
Loss = tf.convert_to_tensor(loss, dtype=tf.float32)
model = load_model('Norm_all_replace_f1_0.84.hdf5')
gradients = K.gradients(Loss, model.input)              #Gradient of output wrt the input of the model (Tensor)


#value of gradient for the first x_test
x_test_1 = inputX
sess = tf.Session()
sess.run(tf.global_variables_initializer())
evaluated_gradients_1 = sess.run(gradients, feed_dict={model.input: x_test_1})
#model_last_layer = sess.run(model.layers[1].output, feed_dict = {model.input: x_test_1})
#model_output = sess.run(model.output, feed_dict = {model.input: x_test_1})
#model_input = sess.run(model.input, feed_dict = {model.input: x_test_1})



evaluated_gradients_1 = np.asarray(evaluated_gradients_1)
print(evaluated_gradients_1)
print (evaluated_gradients_1.shape)

#print (model_last_layer)
#print (model_output)