# -*- coding: utf-8 -*-
"""
Created on Wed Jan  9 15:21:44 2019

@author: jiayi
"""

#import os
#os.environ["THEANO_FLAGS"] = "device=gpu0"


import foolbox
from foolbox.models import KerasModel
from foolbox.attacks import LBFGSAttack
from foolbox.criteria import TargetClass
from keras.layers import Dense, Dropout, LSTM, Embedding
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.models import load_model
from sklearn.model_selection import train_test_split
import keras
import tensorflow as tf
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pandas import Series
from sklearn.preprocessing import MinMaxScaler


def DataNormalization(input_data):
    values = input_data
    # train the normalization
    scaler = MinMaxScaler(feature_range=(0, 1))
    scaler = scaler.fit(values)
    normalized = scaler.transform(values)
    return normalized

def check(label_pred,df_train_y):
    state = label_pred
    df_train_y = pd.DataFrame(df_train_y)
    state = pd.DataFrame(state)
    k1 = 0
    k2 = 0
    s1 = 0
    s2 = 0
    for i in range(len(state)):
        if int(df_train_y.iloc[i]) == 1:
            s1+=1
            if int(state.iloc[i]) == 0:
                k1+=1
        if int(df_train_y.iloc[i]) == 0:
            s2+=1
            if int(state.iloc[i]) == 1:
                k2+=1
    if s1 != 0:
        rate_fn = k1/s1
    else:
        rate_fn = 0
    if s2 != 0:
        rate_fp = k2/s2
    else:
        rate_fp = 0
    rate = (k1+k2)/(len(state))
    print('Error rate:')
    print('false positive:', round(rate_fn,3), 'false nagetive:', round(rate_fp,3), 'error:', round(rate,3))
    return rate_fp,rate_fn,rate   


def plot(t1,t2):
    
    x = np.arange(len(t1))
    
    plt.figure(1)
    
    plt.subplot(211) 
    plt.plot(x, t1)
    
    plt.subplot(212)
    plt.plot(x, t2)
    
    plt.show()





print('loading model...')
model = load_model('check_sensor_0.34-0.96_seq=F.hdf5')
print('loading data...')
df_x = pd.read_csv('attack_x_sensor.csv', encoding='utf-8')
df_x = DataNormalization(df_x)
df_x = np.array(df_x)
df_x = np.expand_dims(df_x,axis = 1)
df_y = pd.read_csv('attack_y_sensor.csv', encoding='utf-8')
print('predicting...')
label_pred = np.array(model.predict_classes(np.array(df_x)))
#label_pred = label_pred.reshape(label_pred.shape[0],label_pred.shape[2])
print('checking accuracy...')
#check false negative,false positive and error rate
fp,fn,rate_err = check(label_pred,df_y)
#plot the graph
t1 = df_y#[500:1500]
t2 = label_pred#[500:1500]
plot(t1,t2)


# =============================================================================
# keras.backend.set_learning_phase(0)
# kmodel = keras.applications.resnet50.ResNet50(weights='imagenet')
# preprocessing = (np.array([104, 116, 123]), 1)
# model = foolbox.models.KerasModel(kmodel, bounds=(0, 255), preprocessing=preprocessing)
# 
# image, label = foolbox.utils.imagenet_example()
# # ::-1 reverses the color channels, because Keras ResNet50 expects BGR instead of RGB
# print(np.argmax(model.predictions(image[:, :, ::-1])), label)
# =============================================================================



keras.backend.set_learning_phase(0)
a = [3]*26
preprocessing = (0, 1)
print('applying attack...')
label_tar = 0*len(label_pred)

attack_x = np.array(df_x).reshape(np.array(df_x).shape[0],np.array(df_x).shape[2])

attack_y = np.array(df_y.iloc[4925][0]).reshape((1,1))


fmodel = KerasModel(model, bounds = (0,1))

#y_fPred = []
#for i in range(len(df_x)):
#    print(i)
#    attack_x = np.array(df_x[i])
#    y_fPred.append(np.argmax(fmodel.predictions(attack_x)))
#    
#    
#fp,fn,rate_err = check(y_fPred,df_y)
##plot the graph
#t1 = df_y#[500:1500]
#t2 = label_pred#[500:1500]
#t3 = y_fPred
#plot(t1,t3)


print(fmodel.predictions(attack_x))

attack = LBFGSAttack(model=fmodel, criterion=TargetClass(0))

adversarial = attack(df_x,label = df_y)
