# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 01:42:30 2018

@author: jiayi
"""

#set environment to use GPU
import os
#os.environ["THEANO_FLAGS"] = "device=gpu0"

from keras.layers import Dense, Dropout, LSTM, Embedding
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.models import load_model
from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np
import copy
import matplotlib.pyplot as plt
from pandas import Series
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import f1_score, precision_score, recall_score


def DataNormalization(input_data):
    values = input_data
    # train the normalization
    scaler = MinMaxScaler(feature_range=(0, 1))
    scaler = scaler.fit(values)
    normalized = scaler.transform(values)
    return normalized


def DataSlection(df2):
    df = copy.copy(df2)
    df2_state = df2['Normal/Attack']
    df.drop('Normal/Attack', axis=1, inplace=True)
    df2_y = []
    df2_x = []
    for i in range(len(df2_state)):
        df2_x.append(df.iloc[i])
        if df2_state.iloc[i] == "Normal":
            df2_y.append(0)
        else:
            df2_y.append(1)
    return df2_y,df2_x

#Comparision
def check(state,df_train_y):
    state = pd.DataFrame(state)
    df_train_y = pd.DataFrame(df_train_y)
    k1 = 0
    k2 = 0
    s1 = 0
    s2 = 0
    for i in range(len(state)):
        if int(df_train_y.iloc[i]) == 1:
            s1+=1
            if int(state.iloc[i]) == 0:
                k1+=1
        if int(df_train_y.iloc[i]) == 0:
            s2+=1
            if int(state.iloc[i]) == 1:
                k2+=1
    if s1 != 0:
        rate_fn = k1/s1
    else:
        rate_fn = 0
    if s2 != 0:
        rate_fp = k2/s2
    else:
        rate_fn = 0
    rate = (k1+k2)/(len(state))
    print('Error rate:')
    print('false positive:', round(rate_fn,4), 'false nagetive:', round(rate_fp,4), 'error:', round(rate,4))
    return rate_fn,rate_fp,rate      


def create_model(input_data):
    input_shape = (1,input_data.shape[1])
    print ('Creating model...')
#    input_cell_length = 51 #change to 26 if use sensor data only
#    timestamp = input_length
    model = Sequential()
    #model.add(Embedding(input_dim = 188, output_dim = 50, input_length = input_length))
    model.add(LSTM(activation="sigmoid", return_sequences=True, units=256,input_shape=input_shape))
    model.add(Dropout(0.5))
    model.add(LSTM(activation="sigmoid", return_sequences=True, units=256))
    #model.add(LSTM(output_dim=256, activation='sigmoid', inner_activation='hard_sigmoid'))
    model.add(Dropout(0.5))
    model.add(Dense(1, activation='sigmoid'))

    print ('Compiling...')
    model.compile(loss='binary_crossentropy',
                  optimizer='adam', #'rmsprop'
                  metrics=['accuracy'])
    return model



#create model without input shape
def create_model_noShape(input_data):
    input_data = np.array(input_data)
    print ('Creating model...')
#    timestamp = input_length
    model = Sequential()
    #model.add(Embedding(input_dim = input_data.shape[1], output_dim = 1, input_length = len(input_data)))
    model.add(LSTM(activation="sigmoid", return_sequences=True, units=256)) #,input_shape=(input_data.shape[1],)
    model.add(Dropout(0.5))
    model.add(LSTM(activation="sigmoid", return_sequences=False, units=256))
    #model.add(LSTM(output_dim=256, activation='sigmoid', inner_activation='hard_sigmoid'))
    model.add(Dropout(0.5))
    model.add(Dense(1, activation='sigmoid'))

    print ('Compiling...')
    model.compile(loss='binary_crossentropy',
                  optimizer='adam', #'rmsprop'
                  metrics=['accuracy'])
    return model



#creat model with moving window
def create_model_win(WINDOW,input_data):
    input_shape = (WINDOW,input_data.shape[1])
    print ('Creating model...')
#    input_cell_length = 51 #change to 26 if use sensor data only
#    timestamp = input_length
    model = Sequential()
    #model.add(Embedding(input_dim = 188, output_dim = 50, input_length = input_length))
    model.add(LSTM(activation="sigmoid", return_sequences=True, units=256,input_shape=input_shape))
    model.add(Dropout(0.5))
    model.add(LSTM(activation="sigmoid", return_sequences=False, units=256))
    #model.add(LSTM(output_dim=256, activation='sigmoid', inner_activation='hard_sigmoid'))
    model.add(Dropout(0.5))
    model.add(Dense(1, activation='sigmoid'))

    print ('Compiling...')
    model.compile(loss='binary_crossentropy',
                  optimizer='adam', #'rmsprop'
                  metrics=['accuracy'])
    return model

def trainDataWin(X_train_no,WINDOW):
    inputX_win = [] 
    for i in range(len(X_train_no)-WINDOW):
        singleWin = X_train_no[i:i+WINDOW]
        singleWin = singleWin.values
        inputX_win.append(singleWin)
    inputX_win = np.array(inputX_win)

def plot(t1,t2):
    
    x = np.arange(len(t1))
    
    plt.figure(1)
    
    plt.subplot(211) 
    plt.plot(x, t1)
    
    plt.subplot(212)
    plt.plot(x, t2)
    
    plt.show()


def predict(input_x):
    model = load_model('Model_attack_sigmoid_sensorOnly.h5')
    output_y = model.predict_classes(np.array(input_x))
    return output_y
    

# =============================================================================
# def headers(input_file):
#     header = list(pd.read_csv(input_file, nrows=1))
#     a= header[1:-1]
#     
#     list_actuator = []
#     list_sensor = []
#     
#     for name in a:
#         if 'MV' in name:
#             print(name)
#             a.remove(name)
#             list_actuator.append(name)
#         if 'P' in name and 'IT' not in name:
#             print(name)
#             a.remove(name)
#             list_actuator.append(name)
#     list_sensor = a
#     return list_sensor, list_actuator
# =============================================================================


# =============================================================================
# 
# #devide the data
# df_whole = pd.read_csv(input_file, encoding='utf-8')
# header = list(pd.read_csv(input_file, nrows=1))
# a= header[1:-1]
# b = header[-1]
# 
# df_actuator = pd.DataFrame()
# list_actuator = []
# 
# for name in header:
#     if 'MV' in name:
#         print(name)
#         #df_whole.drop(name, axis=1, inplace=True)
#         list_actuator.append(name)
#     if 'P' in name and 'IT' not in name:
#         print(name)
#         #df_whole.drop(name, axis=1, inplace=True)
#         list_actuator.append(name)
# 
# df_actuator = df_whole[list_actuator]
# df_actuator.to_csv('attack_x_actuator.csv',index=False)
#         
#         
# df_whole.drop(' Timestamp', axis=1, inplace=True)
# df_whole_y, df_whole_x = DataSlection(df_whole)
# 
# df_whole_x = pd.DataFrame(df_whole_x)
# df_whole_x.to_csv('attack_x.csv',index = False)
# 
# df_whole_y = pd.DataFrame(df_whole_y)
# df_whole_y.to_csv('attack_y.csv',index=False)
# =============================================================================

# =============================================================================
# df = pd.read_csv(input_file, encoding='utf-8')
# df = df.drop(columns = [' Timestamp'])
# df_whole_y, df_whole_x = DataSlection(df)
# df_whole_x = pd.DataFrame(df_whole_x)
# df_whole_y = pd.DataFrame(df_whole_y)
# 
# =============================================================================



#Split the data with shuffle
# =============================================================================
# #X_train, X_test, y_train, y_test = train_test_split(df_whole_x, df_whole_y, train_size=0.7)
#     
# #split the data without shuffle and sensor only
# #df_whole_x = pd.read_csv('attack_x_sensor.csv', encoding='utf-8')
# #df_whole_y = pd.read_csv('attack_y_sensor.csv', encoding='utf-8')
# 
# #split the data without shuffle and all data
# df_whole_x = pd.read_csv('attack_x.csv', encoding='utf-8')
# df_whole_y = pd.read_csv('attack_y.csv', encoding='utf-8')
# 
# 
# df_whole_x = pd.DataFrame(DataNormalization(df_whole_x))
# size = int(len(df_whole_x) * 0.3)
# X_train_no = df_whole_x[size:]
# X_test_no = df_whole_x[:size]
# y_train_no = df_whole_y[size:]
# y_test_no = df_whole_y[:size]
# 
# 
# model = create_model(X_train_no)
# X_train_no = np.array(X_train_no)
# y_train_no = np.array(y_train_no)
# X_train_no = np.expand_dims(X_train_no,axis = 1)
# y_train_no = np.expand_dims(y_train_no,axis = 1)
# 
# print ('Fitting model...')
# hist = model.fit(X_train_no, y_train_no, batch_size=8, epochs=3, validation_split = 0.1)
#  
# #score, acc = model.evaluate(np.array(X_test), np.array(y_test), batch_size=1)
# #print('Test score:', score)
# #print('Test accuracy:', acc)
# 
# 
# 
# XX = df_whole_x
# YY = df_whole_y
# XX = np.expand_dims(XX,axis = 1)
# 
# #prediction
# print('predicting....')
# YY_predict = model.predict_classes(np.array(XX))
# YY_predict = YY_predict.reshape((YY_predict.shape[0],YY_predict.shape[2]))
# #check false negative,false positive and error rate
# print('checking error...')
# rate_fp,rate_fn,rate = check(YY_predict,YY)
# #plot the graph
# t1 = YY#[500:1500]
# t2 = YY_predict#[500:1500]
# plot(t1,t2)
# 
# =============================================================================



#create model without shape
# =============================================================================
# #split the data without shuffle and sensor only
# df_whole_x = pd.read_csv('attack_x_sensor.csv', encoding='utf-8')
# df_whole_y = pd.read_csv('attack_y_sensor.csv', encoding='utf-8')
# 
# df_whole_x = pd.DataFrame(DataNormalization(df_whole_x))
# size = int(len(df_whole_x) * 0.3)
# X_train_no = df_whole_x[size:]
# X_test_no = df_whole_x[:size]
# y_train_no = df_whole_y[size:]
# y_test_no = df_whole_y[:size]
# 
# X_train_no = np.array(X_train_no)
# y_train_no = np.array(y_train_no)
# X_train_no = np.expand_dims(X_train_no,axis = 1)
# 
# #create model
# model = create_model_noShape(X_train_no)
# print ('Fitting model...')
# hist = model.fit(X_train_no, y_train_no, batch_size=8, epochs=3, validation_split = 0.1)
#  
# 
# XX = df_whole_x
# YY = df_whole_y
# XX = np.expand_dims(XX,axis = 1)
# 
# #prediction
# print('predicting....')
# YY_predict = model.predict_classes(np.array(XX))
# #check false negative,false positive and error rate
# print('checking error...')
# rate_fp,rate_fn,rate = check(YY_predict,YY)
# #plot the graph
# t1 = YY#[500:1500]
# t2 = YY_predict#[500:1500]
# plot(t1,t2)
# 
# =============================================================================




#fit model with Moving window
# =============================================================================
# df_whole_x_sensor = pd.read_csv('attack_x_sensor.csv', encoding='utf-8')
# df_whole_x_actuator = pd.read_csv('attack_x_actuator.csv', encoding='utf-8')
# headers = list(pd.read_csv('attack_x.csv', nrows=1))
# headers_sensor = list(pd.read_csv('attack_x_sensor.csv', nrows=1))
# headers_actuator = list(pd.read_csv('attack_x_actuator.csv', nrows=1))
# #df_whole_x = pd.read_csv('attack_x.csv', encoding='utf-8')
# df_whole_y = pd.read_csv('attack_y.csv', encoding='utf-8')
# 
# df_whole_x_actuator = df_whole_x_actuator.replace(0,1.5)
# df_whole_x = pd.concat([df_whole_x_sensor,df_whole_x_actuator], axis=1, sort=False)
# 
# df_whole_x = pd.DataFrame(DataNormalization(df_whole_x))
# size = int(len(df_whole_x) * 0.3/100)*100 
# X_train_no = df_whole_x[(size+19):]
# #X_train_no = pd.DataFrame(DataNormalization(X_train_no))
# X_test_no = df_whole_x[:size]
# #X_test_no = pd.DataFrame(DataNormalization(X_test_no))
# y_train_no = df_whole_y[(size+19):]
# y_test_no = df_whole_y[:size]
# 
# WINDOW=40
# sensorNum = 26
# 
# model = create_model_win(WINDOW,X_train_no)
# 
# #Make a new input including moving windows (a single array into multi window array)
# def windowArray(inputX):
#     inputX_win = [] 
#     for i in range(len(inputX)-WINDOW+1):
#         singleWin = inputX[i:i+WINDOW]
#         singleWin = singleWin.values
#         inputX_win.append(singleWin)
#     inputX_final = np.array(inputX_win)
#     return inputX_final
# 
# 
# #Prepare inputX and inputY to fit model
# #X_train_no = pd.DataFrame(DataNormalization(X_train_no))
# inputX_win = windowArray(X_train_no)
# y_train_no = y_train_no[WINDOW-1:]
# 
# #X_train_no = np.array(X_train_no)
# #y_train_no = np.array(y_train_no)
# #X_train_no = X_train_no.reshape((int(X_train_no.shape[0]/k),k,X_train_no.shape[1]))
# #y_train_no = y_train_no.reshape((int(y_train_no.shape[0]/k),k,y_train_no.shape[1]))
# 
# print ('Fitting model...')
# hist = model.fit(inputX_win, y_train_no, batch_size=20, epochs=1, validation_split = 0.1)
# 
# #Prepare inputX for predicting, and YY for comparison
# XX = windowArray(df_whole_x)
# YY = df_whole_y[WINDOW-1:]
# 
# #prediction
# print('predicting....')
# YY_predict = model.predict_classes(XX)
# 
# #YY_predict = YY_predict.reshape((YY.shape[0],YY.shape[1]))
# #precision,recall = check(YY_predict, YY)
# f1 = f1_score(YY,YY_predict,average='binary')
# precision = precision_score(YY,YY_predict,average='binary')
# recall = recall_score(YY,YY_predict,average='binary')
#  #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#
# print('testing precision, recall, f1')
# print(precision, recall, f1)
# #plot the graph
# t1 = YY#[500:1500]
# t2 = YY_predict#[500:1500]
# plot(t1,t2)
# =============================================================================


 

#New Model without window but change actuator result
#split the data without shuffle 
df_whole_x_sensor = pd.read_csv('attack_x_sensor.csv', encoding='utf-8')
df_whole_x_actuator = pd.read_csv('attack_x_actuator.csv', encoding='utf-8')
headers = list(pd.read_csv('attack_x.csv', nrows=1))
headers_sensor = list(pd.read_csv('attack_x_sensor.csv', nrows=1))
headers_actuator = list(pd.read_csv('attack_x_actuator.csv', nrows=1))
#df_whole_x = pd.read_csv('attack_x.csv', encoding='utf-8')
df_whole_y = pd.read_csv('attack_y.csv', encoding='utf-8')
        
df_whole_x_actuator = df_whole_x_actuator.replace(0,1.5)
df_whole_x = pd.concat([df_whole_x_sensor,df_whole_x_actuator], axis=1, sort=False)
df_whole_x = df_whole_x[headers]
df_whole_x = pd.DataFrame(DataNormalization(df_whole_x))

size = int(len(df_whole_x) * 0.3)
X_train_no = df_whole_x[size:]
X_test_no = df_whole_x[:size]
y_train_no = df_whole_y[size:]
y_test_no = df_whole_y[:size]

X_train_no = np.array(X_train_no)
y_train_no = np.array(y_train_no)
X_train_no = np.expand_dims(X_train_no,axis = 1)

#create model
model = create_model_noShape(X_train_no)
print ('Fitting model...')
hist = model.fit(X_train_no, y_train_no, batch_size=20, epochs=5, validation_split = 0.1)
 

XX = df_whole_x
YY = df_whole_y
XX = np.expand_dims(XX,axis = 1)

#prediction
print('predicting....')
YY_predict = model.predict_classes(np.array(XX))
#check false negative,false positive and error rate
print('checking error...')
f1 = f1_score(YY,YY_predict,average='binary')
precision = precision_score(YY,YY_predict,average='binary')
recall = recall_score(YY,YY_predict,average='binary')
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#
print('testing precision, recall, f1')
print(precision, recall, f1)
#plot the graph
t1 = YY#[500:1500]
t2 = YY_predict#[500:1500]
plot(t1,t2)













# =============================================================================
# 
# #check with moving window
# df_whole_x = pd.read_csv('attack_x_sensor.csv', encoding='utf-8')
# df_whole_y = pd.read_csv('attack_y_sensor.csv', encoding='utf-8')
# WINDOW = 20
# length = len(df_whole_x.iloc[1])
# rangeSize =range(len(df_whole_x)-WINDOW)
# 
# err_fp = 0
# err_fn = 0
# err = 0
# 
# for i in range(50):#rangeSize:
#     i=1
#     input_Xpart = df_whole_x.iloc[i:i+WINDOW]
#     input_Ypart = df_whole_y.iloc[i:i+WINDOW]
# 
#     model = load_model('Model_attack_sigmoid_sensorOnly.h5')
#     predict_Ypart = model.predict_classes(np.array(input_Xpart))
#     rate_fp,rate_fn,rate = check(predict_Ypart,input_Ypart)
#     err_fp += rate_fp*WINDOW
#     err_fn += rate_fn*WINDOW
#     err += rate*100
#     
# Err_fp = err_fp/rangeSize
# Err_fn = err_fn/rangeSize
# Err = err/rangeSize
# print('fp:', Err_fp)
# print('fn:', Err_fn)
# print('er:', Err)
# 
#     
# =============================================================================
    
    





#model.save('Norm_all_replace_f1_0.84.hdf5')
model = load_model('Norm_all_replace_f1_0.84.hdf5')

