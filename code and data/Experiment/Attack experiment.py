# -*- coding: utf-8 -*-
"""
Created on Fri May 10 20:16:23 2019

@author: jiayi
"""

import pandas as pd
import os

#Prepare data
input_folder = 'E:/Yifan_attack_SWaT sys'

headers = list(pd.read_csv('attack_x.csv', nrows=1))
header = []
for i in headers:
    i = i.replace(' ','')
    header.append(i)
header.append('timestamp')

list_files = os.listdir(input_folder)

df_input = pd.DataFrame()

for file_name in list_files:
    col_name = file_name.split('.')[1].split('_')[1]
    print(col_name)
    file_dir = input_folder +'/'+file_name
    df_file = pd.read_csv(file_dir)
    df_input['timestamp'] = df_file['timestamp']
    df_input[col_name] = df_file['value']


df_input = df_input[header]

df_input.to_csv('experiment.csv',index = False)    



#Read saved file
df_file = pd.read_csv('experiment.csv')
df_file['Status'].to_csv('experiment_y.csv',index = False)
df_file = df_file.drop('Status',axis = 1)
df_file = df_file.drop('timestamp',axis = 1)
df_file.to_csv('experiment_x.csv',index = False)


inputX = pd.read_csv('experiment_x.csv')
headers_sensor = list(pd.read_csv('attack_x_sensor.csv', nrows=1))
headers_actuator = list(pd.read_csv('attack_x_actuator.csv', nrows=1))

header_sensor = []
header_actuator = []

for i in headers_sensor:
    i = i.replace(' ','')
    header_sensor.append(i)
    
for i in headers_actuator:
    i = i.replace(' ','')
    header_actuator.append(i)

exp_x_sensor = inputX[header_sensor]
exp_x_act = inputX[header_actuator]
exp_x_sensor.to_csv('experiment_x_sensor.csv',index = False)
exp_x_act.to_csv('experiment_x_actuator.csv',index = False)






