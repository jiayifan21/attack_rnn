# -*- coding: utf-8 -*-
"""
Created on Tue Nov 27 14:44:49 2018

@author: jia yifan
"""

#set environment to use GPU
#import os
#os.environ["THEANO_FLAGS"] = "device=gpu1"

import tensorflow as tf
import pandas as pd
import numpy as np
#import time
import copy
from keras import backend as K
import matplotlib.pyplot as plt
from keras.models import load_model
import keras
#from LSTM_for_SWaT import predict, plot, check
#from keras.optimizers import Adam
from sklearn.preprocessing import MinMaxScaler
import math
from sklearn.metrics import f1_score, precision_score, recall_score



def DataNormalization(input_data):
    values = input_data
    # train the normalization
    scaler = MinMaxScaler(feature_range=(0, 1))
    scaler = scaler.fit(values)
    normalized = scaler.transform(values)
    return normalized

def check(label_pred,df_train_y):
    state = label_pred
    df_train_y = pd.DataFrame(df_train_y)
    state = pd.DataFrame(state)
    k1 = 0
    k2 = 0
    s1 = 0
    s2 = 0
    for i in range(len(state)):
        if int(df_train_y.iloc[i]) == 1:
            s1+=1
            if int(state.iloc[i]) == 0:
                k1+=1
        if int(df_train_y.iloc[i]) == 0:
            s2+=1
            if int(state.iloc[i]) == 1:
                k2+=1
    if s1 != 0:
        rate_fn = k1/s1
    else:
        rate_fn = 0
    if s2 != 0:
        rate_fp = k2/s2
    else:
        rate_fp = 0
    rate = (k1+k2)/(len(state))
    print('Error rate:')
    print('false positive:', round(rate_fn,2), 'false nagetive:', round(rate_fp,2), 'error:', round(rate,2))
    return rate_fp,rate_fn,rate   


def plot(t1,t2):
    
    x1 = np.arange(len(t1))
    x2 = np.arange(len(t2))
    
    plt.figure(1)
    
    plt.subplot(211) 
    plt.plot(x1, t1)
    
    plt.subplot(212)
    plt.plot(x2, t2)
    
    plt.show()


#WINDOW = 50
#threshold=1
#THRESHold = 1
#
#def ctcLoss(y_act, y_pred, input_length, label_length=1):
#    loss = K.ctc_batch_cost(y_act, y_pred, input_length, label_length)
#    return loss
#
#len_x = len(inputX.iloc[1])
#
#input_x = np.array(inputX)
#lengths = [len_x]*len(inputX)
#target = inputY_tar
#finetune=None
#
#learning_rate=100
#num_iterations=1000
#batch_size=len(input_x)
#
#rescale = 0.8
#
#noise = np.random.normal(0,1,input_x.shape)#mean.std dev, shape
#delta = np.zeros((batch_size, len_x), dtype=np.float32)
#delta = np.clip(noise,-10,10)*rescale
#optimizer = Adam(0.0002, 0.5)
#
#
#apply_delta = tf.clip_by_value(delta, -2000, 2000)*self.rescale
#new_input =apply_delta*mask + original
#lengths = tf.Variable(np.zeros(batch_size, dtype=np.int32), name='qq_lengths')
#pass_in = tf.clip_by_value(new_input+noise, -2**15, 2**15-1)
#
#
#
##ctcloss = ctcLoss(inputY_tar,inputY_act,input_length=batch_size, label_length=1)
#
#final_deltas = [None]*batch_size
#now = time.time()
#MAX = num_iterations
#
#y_pred=inputY_tar


######################################################################
# =============================================================================
# def Update_fn(inputX,inputY_pred):
#     inputY_tar = tf.convert_to_tensor(inputY_tar, dtype=tf.float32)
#     inputY_act = tf.convert_to_tensor(np.array(inputY_act), dtype=tf.float32)
#     
#     #noise = tf.random_normal(new_input.shape,stddev=2)
#     #ctcloss = ctcLoss(inputY_tar, inputY_act, input_length=batch_size, label_length=1)
#     
#     #Use log loss as it's binary classification
#     loss = keras.losses.binary_crossentropy(inputY_tar, inputY_act)
#     optimizer = tf.train.AdamOptimizer(learning_rate)
#     
#     grad,var = optimizer.compute_gradients(loss, [delta])[0]
#     #train = optimizer.apply_gradients([(tf.sign(grad),var)])
#     
# =============================================================================

# =============================================================================
# model = load_model('check_sensor_0.34-0.96_seq=F.hdf5')
# 
# inputX = pd.read_csv('attack_x_sensor.csv', encoding='utf-8')
# 
# inputY_act = model.predict_classes(inputX)
# inputY_tar = [0]*len(inputX)
# inputY_act = [1]*len(inputX)
# 
# #t1 = inputY_act#[500:1500]
# #t2 = inputY_tar#[500:1500]
# #plot(t1,t2)
# 
# # Set up the Adam optimizer to perform gradient descent for us
# learning_rate=100
# num_iterations=1000
# delta =  np.random.normal(-1,1,inputX.shape)
# 
# 
# loss_value = 0
# #optimizer = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
# 
# for i in range(5):
#     iteration = i
#     now = time.time()
#     
#     loss_value = np.abs(1 / (1 + math.exp(-loss_value)))
#     delta = delta - loss_value
#     inputX = inputX + delta
#     inputX_nor = DataNormalization(inputX)
#     inputX_nor = np.expand_dims(inputX,axis = 1)
#     
#     
#     
#     #ctcloss = ctcLoss(inputY_act, y_pred, input_length=batch_size, label_length=1)
#     
#     inputY_act = model.predict_classes(np.array(inputX_nor))
#     inputY_act = inputY_act[:,0]
#     inputY_act = tf.convert_to_tensor(np.array(inputY_act), dtype=tf.float32)
#     inputY_tar = tf.convert_to_tensor(inputY_tar, dtype=tf.float32)
#     
#     loss =keras.losses.binary_crossentropy(inputY_tar, inputY_act)
#     
#     loss_value = keras.backend.eval(loss)
#     
# #    noise = tf.convert_to_tensor(delta.reshape((len(delta)),delta.shape[2]), dtype=tf.float32)
# #    noise_Tar = tf.convert_to_tensor(noise_tar.reshape((len(delta)),delta.shape[2]), dtype=tf.float32)
#     noiseLevel =np.linalg.norm(delta)
#     
#     print(loss_value,noiseLevel)
# 
# =============================================================================
   ######################################################         


def windowArray(inputX):
    inputX_win = [] 
    for i in range(len(inputX)-WINDOW+1):
        singleWin = inputX[i:i+WINDOW]
        singleWin = singleWin.values
        inputX_win.append(singleWin)
    inputX_final = np.array(inputX_win)
    return inputX_final
def Binary_crossentropy(Y_target, Y_pred_p):
    result = []
    for i in range(len(Y_pred)):
        value = (Y_target[i] * math.log(Y_pred_p[i]) + (1 - Y_target[i]) * math.log(1 - Y_pred_p[i]))/len(Y_pred_p)
        result.append(value[0])
    return result


# =============================================================================
# def adversial_samples(inputX Y, model, epsilon=0.01):
#     headers = list(pd.read_csv('attack_x.csv', nrows=1))
#     headers_sensor = list(pd.read_csv('attack_x_sensor.csv', nrows=1))
# #    inputX = pd.concat([inputX_sensor,inputX_actuator], axis=1, sort=False)
# #    inputX = inputX[headers]
#     inputX = DataNormalization(inputX)
#     inputX =  windowArray(inputX)
# 
#     Y_pred = model.predict_classes(inputX_win)
# 
#     
#     dlt = Y_pred - Y[(WINDOW-1):]#.reshape(inputX.shape[0], 1)
#     direction = np.sign(np.matmul(dlt, np.array(model.get_weights)))
#     
#     XadvSensor = inputX_sensor - epsilon * direction
#     Xadv = pd.concat([XadvSensor,inputX_actuator], axis=1, sort=False)
#     
#     #Xadv =  windowArray(Xadv,WINDOW = 50)
# 
#     return Xadv
# =============================================================================




# =============================================================================
# 
# grads = K.gradients(model.output,model.input)
# directon = np.sign(grads)
# 
# XadvSensor = inputX_sensor - epsilon * direction
# inputX[headers_sensor] = XadvSensor[headeres_sensor]
# Xadv = inputX
# 
# 
# f = np.array([1, 2, 4, 7, 11, 16], dtype=float)
# x = np.arange(f.size)
# x = [[0,2],[2,2],[4,2],[6,2],[4,2],[2,2]]
# 
# c= np.gradient(f,x)
# 
# =============================================================================

# =============================================================================
# file = open('loss.txt','w')
# file.write(str(loss))
# file.close()
# 
# inputX_sensor.to_csv('inputX_sensor.csv',index = False)
# 
# =============================================================================





#Prepare model abd inputX and tagart Y
print('Loading model...')
epsilon=0.1
WINDOW = 100
model = load_model('check_all_win100_f10.83_NormalSensorOnly.hdf5')
model_input_layer = model.layers[0].input  
model_output_layer = model.layers[-1].output

mode = 'attack'

df_whole_x_sensor = pd.read_csv(mode+'_x_sensor.csv', encoding='utf-8')
df_whole_x_actuator = pd.read_csv(mode+'_x_actuator.csv', encoding='utf-8')
headers = list(pd.read_csv(mode+'_x.csv', nrows=1))
headers_sensor = list(pd.read_csv(mode+'_x_sensor.csv', nrows=1))
headers_actuator = list(pd.read_csv(mode+'_x_actuator.csv', nrows=1))
#df_whole_x = pd.read_csv('attack_x.csv', encoding='utf-8')
        
df_whole_x_actuator = df_whole_x_actuator.replace(0,1.5)
df_whole_x = pd.concat([df_whole_x_sensor,df_whole_x_actuator], axis=1, sort=False)
df_whole_x = df_whole_x[headers_sensor]



print('step1-Normalizing...')
inputX = pd.DataFrame(DataNormalization(df_whole_x),columns = headers_sensor)


Y_origin = pd.read_csv(mode+'_y.csv', encoding='utf-8') 
Y_origin = Y_origin.values 
#Y_target = Y_origin.replace(0,2).replace(1,0).replace(2,1)
#Y_target = Y_target.values
            


#apply attack
print('Applying attack...')

###########TEST###################



#Xadv = copy.copy(inputX)
print('step2-adding window...')
inputX_win = windowArray(inputX)
#inputX_win = np.expand_dims(inputX,axis = 1)
print('step3-predicting....')
Y_pred = model.predict_classes(inputX_win)
#Y_pred = Y_pred[1:]
print('step3-predicting probability....')
Y_pred_p = model.predict_proba(inputX_win)
Y_pred_p[Y_pred_p == 0] = 0.0000001
Y_pred_p[Y_pred_p == 1] = 0.9999999
Y_target = Y_origin[(WINDOW-1):]
Y_origin = Y_origin[(WINDOW-1):]




#Get loss matrix
print('step4-get loss...')
loss = Binary_crossentropy(Y_origin, Y_pred_p)

inputX_sensor = inputX[headers_sensor] 
inputX_actuator = inputX[headers_actuator] 

print('step5-get gradient...')
grads_sensor = pd.DataFrame()
grads_actuator = pd.DataFrame()
for i in headers_sensor:
    a = loss
    b = list(inputX_sensor[i])[(WINDOW-1):]
    grads_sensor[i] = np.gradient(np.array(a),np.array(b))
for i in headers_actuator:
    a = loss
    b = list(inputX_actuator[i])[(WINDOW-1):]
    grads_actuator[i] = np.gradient(np.array(a),np.array(b))

grads_sensor_nan = grads_sensor.fillna(0)
grads_actuator_nan = grads_actuator.fillna(0)
direction_sensor = np.sign(grads_sensor_nan)
direction_actuator = np.sign(grads_actuator_nan)

# =============================================================================
# Loss = tf.convert_to_tensor(loss, dtype=tf.float32)
# # = tf.convert_to_tensor(np.array(inputX_sensor), dtype=tf.float32)
# gradients = K.gradients(Loss, model.input)   
# 
# #value of gradient for the first x_test
# inputX_sensor = inputX_sensor.fillna(0)
# x_test = inputX_sensor.values
# x_test = x_test.reshape(len(x_test),1,26)
# sess = tf.Session()
# sess.run(tf.global_variables_initializer())
# evaluated_gradients_1 = sess.run(gradients, feed_dict={model.input:x_test})
# 
# 
# 
# 
# evaluated_gradients_1 = np.asarray(evaluated_gradients_1)
# print(evaluated_gradients_1)
# print (evaluated_gradients_1.shape)
# =============================================================================


print('step6-generate adversarial example...')
XadvSensor = inputX_sensor + epsilon * direction_sensor
XadvActuator = np.abs(np.abs(direction_actuator) - inputX_actuator)
Xadv = pd.concat([XadvSensor,XadvActuator], axis=1, sort=False)
    
#Xadv[headers_sensor] = XadvSensor[headers_sensor]
Xadv = XadvSensor
print('step7-adding window to adv input...')
Xadv_win = windowArray(Xadv)
#Xadv_win =np.expand_dims(Xadv,axis = 1)
print('step8-predicting adv input...')
Yadv_pred = model.predict_classes(Xadv_win)
#Yadv_pred_p = model.predict_proba(Xadv_win)


#Xadv= adversial_samples(inputX_sensor,inputX_actuator, Y_target, model, epsilon=0.01)
#Y = Y_target
#Predict result after attack
#Y_aftAttack = model.predict_class(Xadv)

#YY_predict = YY_predict.reshape((YY.shape[0],YY.shape[1]))
#precision,recall = check(YY_predict, YY)
f1 = f1_score(Y_origin,Y_pred,average='binary')
precision = precision_score(Y_origin,Y_pred,average='binary')
recall = recall_score(Y_origin,Y_pred,average='binary')
print('testing precision, recall, f1')
print(precision, recall, f1)
#plot the graph
t1 = Y_origin#[500:1500]
t2 = Y_pred#[500:1500]
plot(t1,t2)
         
                       
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            